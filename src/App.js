import React, { Component } from "react";
import "./App.css";
import styled from "styled-components";
import SideBar from "./containers/SideBar";
import Messages from "./containers/Messages";

class App extends Component {
  render() {
    return (
      <AppWrapper>
        <Chat>
          <SideBar />
          <Messages />
        </Chat>
        {/* <h1>{this.state.speed}</h1> */}
      </AppWrapper>
    );
  }
}

const AppWrapper = styled.div`
  display: flex;
  min-height: 100vh;
  justify-content: center;
  align-items: center;

  background-color: black;
`;

const Chat = styled.div`
  display: flex;
  justify-content: space-between;

  width: 60vw;
  height: 80vh;
  border: 1px solid gray;
  border-radius: 3%;

  background-color: white;
  overflow: hidden;
`;

export default App;
