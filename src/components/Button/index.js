import styled from "styled-components";

export default styled.button`
  line-height: 14px;
  text-align: center;
  text-decoration: none;
  white-space: nowrap;
  outline: 0;

  display: inline-block;
  border-radius: 10px;
  background-color: white;
  color: white;
  ${({ backgroud }) => backgroud && `background-color: ${backgroud}`};
  margin-bottom: 10px;

  &:disabled {
    cursor: default;
    color: white;
    border-color: grey;
    background-color: grey;
  }
`;
