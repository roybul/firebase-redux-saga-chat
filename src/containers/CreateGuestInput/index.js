import React from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import { createUser } from "../../modules/users/actions";

import Button from "../../components/Button";

const decorate = connect(
  null,
  { createUser }
);

class CreateGuest extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { inputValue: "" };
  }

  render() {
    const { roomId, createUser } = this.props;
    return (
      <div>
        <Input
          type="text"
          onChange={e => this.setState({ inputValue: e.target.value })}
          placeholder="Please type guest name..."
          value={this.state.inputValue}
        />

        <Button
          backgroud="green"
          onClick={() => {
            createUser({ name: this.state.inputValue, roomId: roomId || null });
            this.setState({ inputValue: "" });
          }}
          disabled={!this.state.inputValue}
        >
          {"Create"}
        </Button>
      </div>
    );
  }
}

const Input = styled.input``;

export default decorate(CreateGuest);
