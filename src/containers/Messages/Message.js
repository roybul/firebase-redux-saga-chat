import React from "react";
import styled from "styled-components";

const Message = ({ userName, message, time }) => (
  <MessageWrapper>
    <div>
      <User>{userName}</User>
      <Time>{time}</Time>
    </div>
    <span>{message}</span>
  </MessageWrapper>
);

const MessageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 10px;
  min-height: 40px;
`;
const User = styled.span`
  font-weight: bold;
  margin-right: 5px;
`;
const Time = styled.time`
  color: grey;
`;

export default Message;
