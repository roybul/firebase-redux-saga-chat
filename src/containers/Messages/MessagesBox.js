import React from "react";
import styled from "styled-components";
import Message from "./Message";

class MessagesBox extends React.PureComponent {
  constructor(props) {
    super(props);
    this.messagesWrapperRef = React.createRef();
  }

  componentDidMount() {
    this.scrollDown();
  }

  componentDidUpdate(prevProps, prevState) {
    this.scrollDown();
  }

  scrollDown = () => {
    const messagesWrapper = this.messagesWrapperRef.current;
    messagesWrapper.scrollTop = messagesWrapper.scrollHeight;
  };

  render() {
    const { roomMessages } = this.props;

    return (
      <MessagesWrapper innerRef={this.messagesWrapperRef}>
        {roomMessages.map(({ userName, message, time }) => (
          <Message
            // TODO add normal key
            key={userName + message + time}
            userName={userName}
            message={message}
            time={time}
          />
        ))}
      </MessagesWrapper>
    );
  }
}

const MessagesWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;

  width: 45vw;
  height: 100%;

  overflow: auto;
`;

export default MessagesBox;
