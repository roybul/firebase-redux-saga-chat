import React from "react";
import styled from "styled-components";
import CreateGuestInput from "../CreateGuestInput";

const NoAccess = ({ room, guestName }) => (
  <StyledNoAccess>
    <h2>{guestName ? "Please choose room" : "Please enter guest name"}</h2>
    {!guestName && <CreateGuestInput roomId={room && room.id} />}
  </StyledNoAccess>
);

const StyledNoAccess = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 45vw;
  heght: 100%;
`;

export default NoAccess;
