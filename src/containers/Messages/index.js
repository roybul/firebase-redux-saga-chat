import React from "react";
import moment from "moment";
import { connect } from "react-redux";
import styled from "styled-components";
import { compose, branch, renderComponent } from "recompose";

import { createMessage } from "../../modules/messages/actions";
import { getRoomMessages } from "../../modules/messages/selectors";
import { getCurrentUser } from "../../modules/users/selectors";
import { getRoom } from "../../modules/rooms/selectors";
import NoAccess from "./NoAccess";
import MessagesBox from "./MessagesBox";

const decorate = compose(
  connect(
    state => {
      const { roomId, name } = getCurrentUser(state);
      const roomMessages = getRoomMessages(state, roomId);
      return { room: getRoom(state, roomId), guestName: name, roomMessages };
    },
    { createMessage }
  ),
  branch(
    ({ guestName, room }) => !guestName || !room,
    renderComponent(NoAccess)
  )
);

const Messages = ({ room, guestName, roomMessages, createMessage }) => {
  let messageAreaRef;

  return (
    <Wrapper>
      <Header>
        <Title>{room.title}</Title>
      </Header>

      <MessagesBox roomMessages={roomMessages} />

      <MessageInputWrapper>
        <MessageArea
          innerRef={textarea => {
            messageAreaRef = textarea;
          }}
          placeholder="Enter message...  Press 'Enter' to send  a message :)"
          onKeyPress={e => {
            if (e.key === "Enter") {
              e.preventDefault();
              e.stopPropagation();

              if (messageAreaRef.value) {
                createMessage({
                  roomId: room.id,
                  userName: guestName,
                  message: messageAreaRef.value,
                  time: moment().format("MM-DD HH:mm")
                });
              }

              messageAreaRef.value = "";
            }
          }}
        />
      </MessageInputWrapper>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;
const Header = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 40px;
  width: 100%;
  border-bottom: 1px solid black;
`;

const Title = styled.h3`
  margin: 0;
`;

const MessageInputWrapper = styled.div`
  position: relative;
  height: 70px;
  width: 100%;
  border-top: 1px solid black;
`;
const MessageArea = styled.textarea`
  width: 100%;
  height: 65px;
`;

export default decorate(Messages);
