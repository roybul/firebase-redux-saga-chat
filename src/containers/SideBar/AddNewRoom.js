import React from "react";
import styled from "styled-components";
import Button from "../../components/Button";

class AddNewRoom extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { inputValue: "" };
  }

  render() {
    const { createRoom } = this.props;
    return (
      <div>
        <Input
          type="text"
          onChange={e => this.setState({ inputValue: e.target.value })}
          placeholder="Please type room name..."
          value={this.state.inputValue}
        />

        <Wrapper>
          <Button
            backgroud="green"
            onClick={() => {
              createRoom({ title: this.state.inputValue });
              this.setState({ inputValue: "" });
            }}
            disabled={!this.state.inputValue}
          >
            {"Add"}
          </Button>
        </Wrapper>
      </div>
    );
  }
}

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
`;
const Input = styled.input``;

export default AddNewRoom;
