import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";

import { createRoom } from "../../modules/rooms/actions";
import { updateCurrentUser } from "../../modules/users/actions";
import { getRooms } from "../../modules/rooms/selectors";
import { getCurrentUser } from "../../modules/users/selectors";
import AddNewRoom from "./AddNewRoom";

const decorate = connect(
  state => ({ rooms: getRooms(state), currentUser: getCurrentUser(state) }),
  { createRoom, updateCurrentUser }
);

const SideBar = ({ rooms, currentUser, createRoom, updateCurrentUser }) => {
  return (
    <StyledSideBar>
      <SideBarTitle>{"Rooms"}</SideBarTitle>
      <AddNewRoom createRoom={createRoom} />
      <RoomsWrapper>
        {rooms.map(room => (
          <Room
            onClick={() => updateCurrentUser({ roomId: room.id })}
            key={room.id}
            roomSelected={currentUser.roomId === room.id}
          >{`#${room.title}`}</Room>
        ))}
      </RoomsWrapper>
    </StyledSideBar>
  );
};

const StyledSideBar = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  width: 15vw;
  heght: 100%;
  background-color: #4b0082;
`;

const SideBarTitle = styled.h3`
  margin-bottom: 5px;
  color: #a9a9a9;
`;
const RoomsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  cursor: pointer;
`;

const Room = styled.span`
  color: #f5f5f5;
  ${({ roomSelected }) => roomSelected && "color: #7FFF00"};
  margin-bottom: 1px;
`;

export default decorate(SideBar);
