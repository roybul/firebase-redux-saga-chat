import * as firebase from "firebase";
import ReduxSagaFirebase from "redux-saga-firebase";

// Initialize Firebase
var config = {
  apiKey: "AIzaSyCaSo2wt3_0JRsroU7bJPNj5Twp6lZu5eY",
  authDomain: "react-redux-saga-chat.firebaseapp.com",
  databaseURL: "https://react-redux-saga-chat.firebaseio.com",
  projectId: "react-redux-saga-chat",
  storageBucket: "",
  messagingSenderId: "282288186706"
};

const firebaseChat = firebase.initializeApp(config);

export const reduxSagaFirebase = new ReduxSagaFirebase(firebaseChat);
