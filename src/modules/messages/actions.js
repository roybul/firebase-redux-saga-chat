import createAction from "../../utils/createAction";

export const catchMessageCreationInBase = createAction(
  "messages/CATH_MESSAGE_CRETION_IN_BASE"
);

export const createMessage = createAction("messages/CREATE_MESSAGE");
