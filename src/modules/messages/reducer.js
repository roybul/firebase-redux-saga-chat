import { catchMessageCreationInBase } from "./actions";

const MESSAGES_STATE = {};

const messages = (state = MESSAGES_STATE, action) => {
  switch (action.type) {
    case catchMessageCreationInBase.type:
      return { ...state, ...action.payload };

    default:
      return state;
  }
};

export default messages;
