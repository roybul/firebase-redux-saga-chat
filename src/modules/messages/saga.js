import { put, take, call, takeEvery, fork } from "redux-saga/effects";
import createEventChannel from "../../utils/createEventChannel";
import pushToBase from "../../utils/pushToBase";
import { catchMessageCreationInBase, createMessage } from "./actions";

function* watchMessagesDataBaseUpdateSaga() {
  const messagesDataBaseUpdateChannel = yield call(
    createEventChannel,
    "messages",
    "value"
  );
  while (true) {
    const payload = yield take(messagesDataBaseUpdateChannel);
    yield put(catchMessageCreationInBase(payload));
  }
}

function* createMessageSaga({ payload }) {
  try {
    yield call(pushToBase, `messages/${payload.roomId}`, payload, true);
  } catch (e) {
    //TODO do something with the error, such as dispatching an error action with yield put
  }
}

export default function* watchMessagesActions() {
  yield fork(watchMessagesDataBaseUpdateSaga);

  yield takeEvery(createMessage.type, createMessageSaga);
}
