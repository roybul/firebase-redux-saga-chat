import { values } from "lodash/fp";
import { createSelector } from "reselect";

export const getMessagesByRoomId = state => state.messages;

export const getRoomMessages = createSelector(
  getMessagesByRoomId,
  (_, roomId) => roomId,
  (messages, id) => values(messages[id]) || []
);
