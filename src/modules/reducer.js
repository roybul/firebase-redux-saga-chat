import { combineReducers } from "redux";
import users from "./users/reducer";
import currentUser from "./users/currentUserReducer";
import rooms from "./rooms/reducer";
import messages from "./messages/reducer";
export default combineReducers({
  currentUser,
  users,
  rooms,
  messages
});
