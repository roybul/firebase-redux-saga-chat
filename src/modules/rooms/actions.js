import createAction from "../../utils/createAction";

export const updateRoomsInStore = createAction("rooms/UPDATE_ROOMS_IN_STORE");

export const createRoom = createAction("rooms/CREATE_ROOM");
