import { mapValues } from "lodash/fp";
import { updateRoomsInStore } from "./actions";
const unCappedMapValues = mapValues.convert({ cap: false });

const ROOMS_INITIAL_STATE = {};

const roomsReducer = (state = ROOMS_INITIAL_STATE, action) => {
  switch (action.type) {
    case updateRoomsInStore.type: {
      return {
        ...state,
        ...unCappedMapValues(
          (value, key) => ({ id: key, ...value }),
          action.payload
        )
      };
    }

    default:
      return state;
  }
};

export default roomsReducer;
