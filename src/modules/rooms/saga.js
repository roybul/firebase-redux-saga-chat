import { put, takeEvery, call, fork, take } from "redux-saga/effects";
import createEventChannel from "../../utils/createEventChannel";
import pushToBase from "../../utils/pushToBase";
import { updateRoomsInStore, createRoom } from "./actions";

function* watchRoomsDataBaseUpdateSaga() {
  const roomsDataBaseUpdateChannel = createEventChannel("rooms", "value");
  while (true) {
    const payload = yield take(roomsDataBaseUpdateChannel);
    yield put(updateRoomsInStore(payload));
  }
}

function* createRoomSaga({ payload }) {
  try {
    yield call(pushToBase, "rooms", payload, true);
  } catch (e) {
    //TODO do something with the error, such as dispatching an error action with yield put
  }
}

export default function* watchRoomsActions() {
  yield fork(watchRoomsDataBaseUpdateSaga);

  yield takeEvery(createRoom.type, createRoomSaga);
}
