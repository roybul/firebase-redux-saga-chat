import { values } from "lodash/fp";
import { createSelector } from "reselect";

export const getRoomsById = state => state.rooms;

export const getRoom = (state, id) => state.rooms[id];

export const getRooms = createSelector(getRoomsById, roomsById =>
  values(roomsById)
);
