import { spawn } from "redux-saga/effects";
import "../firebaseInit";
import watchUserActions from "./users/saga";
import watchMessagesActions from "./messages/saga";
import watchRoomsActions from "./rooms/saga";

export default function* globalSaga() {
  yield spawn(watchUserActions);
  yield spawn(watchMessagesActions);

  yield spawn(watchRoomsActions);
}
