import createAction from "../../utils/createAction";

export const catchUserCreationInBase = createAction(
  "users/CATH_USER_CRETION_IN_BASE"
);

export const createUser = createAction("users/CREATE_USER");

export const updateCurrentUser = createAction("users/UPDATE_CURRENT_USER");
