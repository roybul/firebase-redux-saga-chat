import { updateCurrentUser } from "./actions";
const CURENT_USER_STATE = {
  name: null,
  id: null,
  roomId: null
};

const currentUser = (state = CURENT_USER_STATE, action) => {
  switch (action.type) {
    case updateCurrentUser.type:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};

export default currentUser;
