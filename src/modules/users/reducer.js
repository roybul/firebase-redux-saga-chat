import { catchUserCreationInBase } from "./actions";

const USERS_STATE = {};

const users = (state = USERS_STATE, action) => {
  switch (action.type) {
    case catchUserCreationInBase.type:
      return { ...state, ...action.payload };

    default:
      return state;
  }
};

export default users;
