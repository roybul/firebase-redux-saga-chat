import { put, take, call, takeEvery, fork } from "redux-saga/effects";
import createEventChannel from "../../utils/createEventChannel";
import pushToBase from "../../utils/pushToBase";
import {
  catchUserCreationInBase,
  createUser,
  updateCurrentUser
} from "./actions";

function* watchUsersDataBaseUpdateSaga() {
  const userDataBaseUpdateChannel = yield call(
    createEventChannel,
    "users",
    "value"
  );
  while (true) {
    const payload = yield take(userDataBaseUpdateChannel);
    yield put(catchUserCreationInBase(payload));
  }
}

function* createUserSaga({ payload }) {
  try {
    const { key } = yield call(pushToBase, "users", payload, true);

    yield put(updateCurrentUser({ id: key, ...payload }));
  } catch (e) {
    //TODO do something with the error, such as dispatching an error action with yield put
  }
}

export default function* watchUserActions() {
  yield fork(watchUsersDataBaseUpdateSaga);

  yield takeEvery(createUser.type, createUserSaga);
}
