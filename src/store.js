import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";

import globalReducer from "./modules/reducer";
import gloabalSaga from "./modules/saga";

// create the saga middleware
const sagaMiddleware = createSagaMiddleware();
// mount it on the Store
const store = createStore(
  globalReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(sagaMiddleware)
);

// then run the saga
sagaMiddleware.run(gloabalSaga);

export default store;
