import { eventChannel } from "redux-saga";
import * as firebase from "firebase";

export default function createEventChannel(
  dataBasePath,
  event = "child_added"
) {
  const listener = eventChannel(emit => {
    firebase
      .database()
      .ref(dataBasePath)
      .on(event, data => emit(data.val()));

    return () => firebase.database.ref(dataBasePath).off(listener);
  });

  return listener;
}
