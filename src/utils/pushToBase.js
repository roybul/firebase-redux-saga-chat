import * as firebase from "firebase";
import updateInBase from "./updateInBase";

export default function pushToBase(path, payload, withPush = false) {
  const databaseRef = firebase.database().ref(path);

  // Creating my own push.
  const newPostKey = databaseRef.push().key;
  const push = updateInBase(`${path}/${newPostKey}`, payload);

  return withPush ? { key: newPostKey, push: push } : databaseRef.set(payload);
}
