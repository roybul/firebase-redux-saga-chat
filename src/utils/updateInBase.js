import * as firebase from "firebase";

export default function unpdateInBase(path, payload) {
  const databaseRef = firebase.database().ref(path);

  return databaseRef.update(payload);
}
